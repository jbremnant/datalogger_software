import sys, os

"""
To Test:
  >>> import dl
  >>> df = pd.DataFrame(list(dl.DataLoggerCSV(path)), columns=['ts','gx','gy','gz','ax','ay','az','mx','my','mz'])
"""

class DataLoggerCSV(object):
  """This wrapper around the file handle, pools data into single row"""

  def __init__(self, path, **kwargs):
    if not os.path.exists(path):
      raise ValueError("{0} does not exist".format(path))

    self.fh = open(path, 'r') 

    self.store = {
      # serial number
      'serial' :
      {
        'vals' : [None,None,None],  # low, middle, high
        'func' : self._update_serial,
      },
      # gyro and accel
      'ga' :
      {
        'vals' : [None, None, None, None, None, None],
        'func' : self._update_ga,
      },
      'mag' :
      {
        'vals' : [None, None, None],
        'func' : self._update_mag,
      },
      'agt' :
      {
        'vals' : [None],
        'func' : self._update_agt,
      },
      'prs' :
      {
        'vals' : [None],
        'func' : self._update_prs,
      },
      'prt' :
      {
        'vals' : [None],
        'func' : self._update_prt,
      },
      'gps' : # 9253771,gps,$GPGGA,151737.094,,,,,0,00,,,M,,M,,*73
      {
        'vals' : [None],
        'func' : self._update_gps,
      }, 
      '1pps' :
      {
        'vals' : [None],
        'func' : self._update_1pps,
      },
      'vbat' :
      {
        'vals' : [None],
        'func' : self._update_vbat,
      },
      'tck' :
      {
        'vals' : [None],
        'func' : self._update_tck,
      },
    }
    self.data_group = ['ga','mag']
    if 'data_group' in kwargs:
      userdg = kwargs['data_group']
      if isinstance(userdg, str): userdg = userdg.split(",")
      if any( [i not in self.store for i in userdg] ):
        raise KeyError("one or more of data_group not found. Choose from {0}".format(
          ",".join(self.store.keys())) )
      self.data_group = userdg

    self.systick_hz       = 2621440.0   # 41943040 / 16
    self.systick_overflow = 2**24 # systick timer is 24 bit wide
    self.tickstart, self.systick0 = (None, None)
    self.ts     = None
    self.tsdiff = None

    self.gyro_const = (1.0/32768.0)*2000.0 # 2^15 fractions of full scale, 2000 dps
    # self.acc_const  = (1.0/(2**16 / 32.0 * 2/3)) # 16 bits, -/+16G scale, funky 1.5 div
    self.acc_const  = (1.0/(2**16 / 32.0)) # 16 bits, -/+24G scale for conversion purposes
    self.mag_const  = (1.0/2**16)
    self.vbat_const = (1.0/2**16)*2.0*3.3

  def _update_ts(self, systick):
    systick = float(systick)
    if self.ts is None:
      self.tickstart = self.systick0 = systick
      self.ts = 0
      self.tsdiff = 0
      return
    if systick > self.systick0:
      self.systick0 = self.systick_overflow - self.systick0

    # SysTick timer counts down
    self.tsdiff    = (self.systick0 - systick) / self.systick_hz
    self.ts       += self.tsdiff
    self.systick0  = systick

  def _update_serial(self, vals):  
    self.store['serial']['vals'] = vals

  def _update_ga(self, vals):  
    gx,gy,gz,ax,ay,az = [float(i) for i in vals]
    gx *= self.gyro_const
    gy *= self.gyro_const
    gz *= self.gyro_const
    ax *= self.acc_const
    ay *= self.acc_const
    az *= self.acc_const
    self.store['ga']['vals'] = [gx,gy,gz,ax,ay,az]

  def _update_mag(self, vals):  
    self.store['mag']['vals'] = [float(i)*self.mag_const for i in vals]

  def _update_agt(self, vals):
    self.store['agt']['vals'] = [25.0+float(vals[0])/16.0] # in Celsius

  def _update_prs(self, vals):
    self.store['prs']['vals'] = [float(vals[0])/4096] # in millibar

  def _update_prt(self, vals):
    self.store['prt']['vals'] = [42.5+float(vals[0])/480] # in Celsius

  def _update_gps(self, vals):
    self.store['gps']['vals'] = vals

  def _update_1pps(self, vals):
    self.store['1pps']['vals'] = vals

  def _update_vbat(self, vals):
    self.store['vbat']['vals'] = [float(vals[0])*self.vbat_const]

  def _update_tck(self, vals):
    self.store['tck']['vals'] = vals

  def _getrow(self):
    """
    Processes each line and returns desired data items
    """
    line = next(self.fh)
    if line is None:
      self.fh.close()
      return None
    tok  = line.strip().split(",") 
    syscnt, kind = tok[0:2]
    if kind not in self.data_group:  
      return None
    self._update_ts(syscnt)
    if kind in self.store:
      func = self.store[kind]['func']
      func(tok[2:]) 
    res = [self.ts]
    for k in self.data_group: 
      if k in self.store:
        res += self.store[k]['vals']
    return res

  def __iter__(self):
    return self

  def next(self):
    row = None
    while row is None:
      row = self._getrow()
      if self.fh.closed:
        raise StopIteration
    return row
  
  def __enter__(self):
    return self

  def __exit__(self):
    if not self.fh.closed:
      self.fh.close()
